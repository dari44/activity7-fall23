package com.example;

public class BagelSandwich implements ISandwich{
    private String filling;

    public BagelSandwich(){
        this.filling = "";
    }

    public void addFilling(String filling){
        this.filling = filling;
    }

    public String getFilling(){
        return this.filling;
    }

    public boolean isVegetarian(){
        throw new UnsupportedOperationException();
    }
}

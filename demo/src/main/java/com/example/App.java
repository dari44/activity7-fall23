package com.example;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        VegetarianSandwich s = new TofuSandwich();
        //System.out.println(s.getFilling());

        System.out.println(s instanceof ISandwich);
        System.out.println(s instanceof VegetarianSandwich);
        
        s.addFilling("chicken"); //throws exception
        System.out.println(s.getFilling());
        System.out.println(s.isVegetarian());

        VegetarianSandwich c = new CaesarSandwich();
        System.out.println(c.isVegetarian());
    }
}

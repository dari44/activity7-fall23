package com.example;

abstract public class VegetarianSandwich implements ISandwich{
    public String filling;

    public void addFilling(String filling){
        String[] no = {"chicken", "beef", "fish", "meat", "pork"};
        for (String s : no){
            if (filling.equals(s)){
                throw new IllegalArgumentException("no");
            }
            else{
                this.filling = filling;
            }
        }
    }

    final public boolean isVegetarian(){
        return true;
    }

    public boolean isVegan(){
        if (this.filling.equals("egg") || this.filling.equals("cheese")){
            return false;
        }
        return true;
    }

    abstract public String getProtein();
}

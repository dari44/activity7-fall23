package com.example;

public class TofuSandwich extends VegetarianSandwich{
    public TofuSandwich(){
        super.filling = "Tofu";
    }

    public String getProtein(){
        return super.filling;
    }

    public String getFilling(){
        return super.filling;
    }
}

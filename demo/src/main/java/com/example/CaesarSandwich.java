package com.example;

public class CaesarSandwich extends VegetarianSandwich{
    public CaesarSandwich(){
        super.filling = "Caesar dressing";
    }

    public String getProtein(){
        return "Anchovies";
    }

    public String getFilling(){
        return super.filling;
    }
}


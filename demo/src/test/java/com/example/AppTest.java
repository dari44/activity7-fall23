package com.example;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */
    @Test
    public void TofuSandwichCreationTest(){
        VegetarianSandwich s = new TofuSandwich();
        assertEquals("Tofu", s.getFilling());
        assertEquals("Tofu", s.getProtein());
        assertEquals(true, s.isVegetarian());
        assertEquals(true, s.isVegan());
    }

    @Test
    public void CeasarSandwichCreationTest(){
        VegetarianSandwich s = new CaesarSandwich();
        assertEquals("Caesar dressing", s.getFilling());
        assertEquals("Anchovies", s.getProtein());
        assertEquals(true, s.isVegetarian());
        assertEquals(true, s.isVegan());
    }

    @Test
    public void addFillingIllegalArgumentExceptionTest(){
        try{
            VegetarianSandwich s = new TofuSandwich();
            s.addFilling("chicken");
            fail("exception not thrown");
        }
        catch(Exception e ){}
    }

    @Test
    public void isVeganTest(){
        VegetarianSandwich s = new TofuSandwich();
        s.addFilling("egg");
        assertEquals(false, s.isVegan());
    }

}
